<?php
define('DEBUG', false);
define('START_TIME', microtime(true));
define('START_MEMORY_USAGE', memory_get_usage());
define('BASE_DIR', dirname(__FILE__));
define('PUBLIC_DIR', dirname(__FILE__) . '/../public/');
define('AJAX_REQUEST', strtolower(getenv('HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest');

$inc = get_include_path() . 
	PATH_SEPARATOR . BASE_DIR . '/../' .
	PATH_SEPARATOR . BASE_DIR . '/../libraries/';
	
set_include_path($inc); 
spl_autoload_extensions('.php');
spl_autoload_register();

$c = new \App\Config\Main;

new \App\Core\Session($c->session_name, $c->session_timeout, true);

\App\Core\Dispatcher::go($c);
